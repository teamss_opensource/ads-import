<?php
use tss\sensavi\ads;
require_once(__DIR__."/constants.php");
require_once(__DIR__."/class/provider.php");

$providerQuery = $_REQUEST["provider"];
$provider=null;
$debug=isset($_REQUEST["debug"]);

log_echo(print_r($_REQUEST,true));
log_echo(print_r($_SERVER['DOCUMENT_ROOT'],true));
log_echo(__DIR__);
switch ($providerQuery)
{
    case "facebook":{
        log_echo("init module facebook");
            require_once(__DIR__."/providers/facebook.php");
        log_echo("create component");
            $provider= new ads\facebook($debug);
    }break;
    case "jivo":{
        log_echo("init module jivo");
        require_once(__DIR__."/providers/jivo.php");
        log_echo("create component");
            $provider= new ads\jivo($debug);
    }break;
    default:
        {
            log_echo("init module universal");
            require_once(__DIR__."/providers/universal.php");
            log_echo("create component");
            $provider= new ads\universal($debug);
        }break;
}
log_echo("init end");

$provider->listen();

function log_echo($data)
{
    global $debug;
    if($debug===true)
        echo $data."<br>";
}
