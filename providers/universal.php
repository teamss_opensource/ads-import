<?php


namespace tss\sensavi\ads;


class universal extends provider
{

    public function __construct($debug)
    {
        parent::__construct($debug);
        $this->name = "universal";
        $this->on("default","saveToBD");

    }

    public function handleData($data)
    {
        return "default";
    }

}