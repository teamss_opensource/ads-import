<?php


namespace tss\sensavi\ads;


class facebook extends provider
{
    public function __construct($debug)
    {
        parent::__construct($debug);
        $this->name = "facebook";
        $this->on("default",'saveToBD');
    }

    public function handleData($data)
    {

        $challenge="";
        $verify_token="";

        if (isset($_REQUEST['hub_challenge']))
            $challenge = $_REQUEST['hub_challenge'];

        if (isset($_REQUEST['hub_verify_token']))
            $verify_token = $_REQUEST['hub_verify_token'];

        if ($verify_token === access_token) {
            echo $challenge;
        }
        else
            die(401);

        return "default";
    }
}