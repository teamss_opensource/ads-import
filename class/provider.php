<?php


namespace tss\sensavi\ads;


class provider
{

    protected $name;
    private $hooks;
    private $dbconn;
    private $debug;

    public function __construct($debug)
    {
        $this->log("run constructor");
        $this->hooks = array();
        $this->debug = $debug;

        $this->log("begin connect to BD");
        //Подключение к базе
        $this->dbconn = pg_connect("host=".db_host." dbname=".db_name." user=".db_user." password=".db_password)
        or die('Не удалось соединиться: ' . pg_last_error());

        $this->log("end constructor");
    }

    public function __destruct()
    {
        pg_close($this->dbconn);
    }


    private function getCallbacks($name)
    {
        return isset($this->hooks[$name]) ? $this->hooks[$name] : array();
    }

    public function on($name, $callback)
    {
        if (!is_callable($callback, true)) {
            throw new \InvalidArgumentException(sprintf('Invalid callback: %s.', print_r($callback, true)));
        }

        $this->hooks[$name][] = $callback;
    }

    public function listen()
    {
        $data = json_decode(file_get_contents('php://input'), true);

        if (empty($data)) {
            return false;
        }

        $event = $this->handleData($data);

        if($event==null)
            return false;

        foreach($this->getCallbacks($event) as $callback) {
            call_user_func(array($this,$callback), $data);
        }
    }

    public function handleData($data)
    {
        return null;
    }

    public function respond($response = array())
    {
        if (!empty($response)) {
            header('Content-Type: application/json; charset=utf-8');
            die(json_encode($response));
        }
    }

    public function saveToBD($data)
    {
        $dataForInsert = json_encode($data);

        $query = 'INSERT INTO ads_data ( type, data) VALUES ( $1 , $2 )';
        $result = pg_query_params($this->dbconn,$query, array($this->name,$dataForInsert)) or die('Ошибка запроса: ' . pg_last_error());
    }

    public function log($data)
    {
        if($this->debug)
            echo $data."<br>";
    }
}